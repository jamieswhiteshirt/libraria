package com.jamieswhiteshirt.libraria

import com.jamieswhiteshirt.libraria.common.CommonProxy
import net.minecraftforge.fml.common.Mod
import net.minecraftforge.fml.common.SidedProxy
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent

@Mod(
        name = "Libraria",
        modid = Libraria.MODID,
        version = Libraria.VERSION,
        modLanguageAdapter = "net.shadowfacts.forgelin.KotlinAdapter"
)
object Libraria {
    const val MODID = "libraria"
    const val VERSION = "0.0.0.1"
    const val CLIENT_PROXY = "com.jamieswhiteshirt.libraria.client.ClientProxy"
    const val SERVER_PROXY = "com.jamieswhiteshirt.libraria.server.ServerProxy"

    @SidedProxy(clientSide = CLIENT_PROXY, serverSide = SERVER_PROXY, modId = MODID)
    lateinit var proxy: CommonProxy private set

    @Mod.EventHandler
    fun preInit(e: FMLPreInitializationEvent) {
        proxy.preInit(e)
    }

    @Mod.EventHandler
    fun init(e: FMLInitializationEvent) {
        proxy.init(e)
    }

    @Mod.EventHandler
    fun postInit(e: FMLPostInitializationEvent) {
        proxy.postInit(e)
    }
}
