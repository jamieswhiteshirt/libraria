package com.jamieswhiteshirt.libraria.api

import net.minecraft.util.EnumFacing
import net.minecraft.util.IStringSerializable

enum class EnumHorizontalRelativeFacing(private val _name: String, private val oppositeOrdinal: Int, private val rotation: Int) : IStringSerializable {
    LEFT("left", 1, -1),
    RIGHT("right", 0, 1);

    companion object {
        fun getFacing(ordinal: Int) = values()[ordinal]
    }

    val opposite: EnumHorizontalRelativeFacing get() = getFacing(oppositeOrdinal)

    override fun getName(): String = _name

    fun map(facing: EnumFacing): EnumFacing = if (facing.horizontalIndex != -1) {
        EnumFacing.getHorizontal((facing.horizontalIndex + rotation) and 3)
    } else {
        facing
    }

    fun unmap(facing: EnumFacing): EnumFacing = if (facing.horizontalIndex != -1) {
        EnumFacing.getHorizontal((facing.horizontalIndex - rotation) and 3)
    } else {
        facing
    }
}