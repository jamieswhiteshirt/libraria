package com.jamieswhiteshirt.libraria.server

import com.jamieswhiteshirt.libraria.common.CommonProxy
import net.minecraftforge.common.config.Configuration
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

@SideOnly(Side.SERVER)
class ServerProxy : CommonProxy() {
    override lateinit var config: ServerConfig private set
    override lateinit var features: ServerFeatures private set

    override fun loadConfiguration(configuration: Configuration) {
        config = ServerConfig(configuration)
    }

    override fun loadFeatures() {
        features = ServerFeatures(config)
    }
}
