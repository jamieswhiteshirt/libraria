package com.jamieswhiteshirt.libraria.server

import com.jamieswhiteshirt.libraria.common.CommonConfig
import net.minecraftforge.common.config.Configuration
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

@SideOnly(Side.SERVER)
class ServerConfig(configuration: Configuration) : CommonConfig(configuration)
