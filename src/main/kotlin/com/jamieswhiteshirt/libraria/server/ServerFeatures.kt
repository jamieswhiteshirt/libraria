package com.jamieswhiteshirt.libraria.server

import com.jamieswhiteshirt.libraria.common.CommonFeatures
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

@SideOnly(Side.SERVER)
class ServerFeatures(config: ServerConfig) : CommonFeatures(config)
