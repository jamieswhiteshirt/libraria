package com.jamieswhiteshirt.libraria.client

import com.jamieswhiteshirt.libraria.common.CommonProxy
import net.minecraftforge.common.config.Configuration
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

@SideOnly(Side.CLIENT)
class ClientProxy : CommonProxy() {
    override lateinit var config: ClientConfig private set
    override lateinit var features: ClientFeatures private set

    override fun loadConfiguration(configuration: Configuration) {
        config = ClientConfig(configuration)
    }

    override fun loadFeatures() {
        features = ClientFeatures(config)
    }

    override fun preInit(e: FMLPreInitializationEvent) {
        super.preInit(e)

        features.registerRenderers()
    }
}
