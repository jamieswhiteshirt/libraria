package com.jamieswhiteshirt.libraria.client

import com.jamieswhiteshirt.libraria.client.eventhandler.ViewEventHandler
import com.jamieswhiteshirt.libraria.common.CommonFeatures
import net.minecraft.client.renderer.block.model.ModelResourceLocation
import net.minecraft.item.Item
import net.minecraftforge.client.model.ModelLoader
import net.minecraftforge.common.MinecraftForge
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

@SideOnly(Side.CLIENT)
class ClientFeatures(config: ClientConfig) : CommonFeatures(config) {
    fun registerRenderers() {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(enormousBookClosed), 0, ModelResourceLocation("libraria:enormous_book_closed", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(charredBookshelf), 0, ModelResourceLocation("libraria:charred_bookshelf", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(charredPlanks), 0, ModelResourceLocation("libraria:charred_planks", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(smolderingBookshelf), 0, ModelResourceLocation("libraria:charred_bookshelf", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(smolderingPlanks), 0, ModelResourceLocation("libraria:charred_planks", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(paperPile), 0, ModelResourceLocation("libraria:paper_pile", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(ashPile), 0, ModelResourceLocation("libraria:ash_pile", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(marmor), 0, ModelResourceLocation("libraria:marmor", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(marmorTile), 0, ModelResourceLocation("libraria:marmor_tile", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(marmorTileStairs), 0, ModelResourceLocation("libraria:marmor_tile_stairs", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(marmorRailing), 0, ModelResourceLocation("libraria:marmor_railing", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(marmorPillar), 0, ModelResourceLocation("libraria:marmor_pillar", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(marmorBookshelf), 0, ModelResourceLocation("libraria:marmor_bookshelf", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(oakRailing), 0, ModelResourceLocation("libraria:oak_railing", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(wetBookshelf), 0, ModelResourceLocation("libraria:wet_bookshelf", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(wetPlanks), 0, ModelResourceLocation("libraria:wet_planks", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(soakedBookshelf), 0, ModelResourceLocation("libraria:soaked_bookshelf", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(soakedPlanks), 0, ModelResourceLocation("libraria:soaked_planks", "inventory"))
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(puddle), 0, ModelResourceLocation("libraria:puddle", "inventory"))
    }

    override fun registerEventHandlers() {
        super.registerEventHandlers()

        MinecraftForge.EVENT_BUS.register(ViewEventHandler(this))
    }
}
