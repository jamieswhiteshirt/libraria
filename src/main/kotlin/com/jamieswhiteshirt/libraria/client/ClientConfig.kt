package com.jamieswhiteshirt.libraria.client

import com.jamieswhiteshirt.libraria.common.CommonConfig
import net.minecraftforge.common.config.Configuration
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

@SideOnly(Side.CLIENT)
class ClientConfig(configuration: Configuration) : CommonConfig(configuration)
