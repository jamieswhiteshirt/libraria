package com.jamieswhiteshirt.libraria.client.eventhandler

import com.jamieswhiteshirt.libraria.client.ClientFeatures
import net.minecraft.client.Minecraft
import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.EnumFacing
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.MathHelper
import net.minecraft.util.math.RayTraceResult
import net.minecraft.world.World
import net.minecraftforge.client.event.EntityViewRenderEvent
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent
import net.minecraftforge.fml.common.gameevent.TickEvent

class ViewEventHandler(val features: ClientFeatures) {
    companion object {
        const val MAX_LOOK_TIME = 60
    }
    private var prevLookTime: Int = 0
    private var lookTime: Int = 0

    private val interpolatedLookTime: Float get() {
        val partialTicks = Minecraft.getMinecraft().renderPartialTicks
        return (1.0F - partialTicks) * prevLookTime + partialTicks * lookTime
    }

    fun scaleFactor(lookTime: Float): Float = (1.075F * MAX_LOOK_TIME - lookTime) / (1.075F * MAX_LOOK_TIME)

    fun isOnBook(world: World, rayTraceResult: RayTraceResult?, entity: Entity): Boolean {
        if (rayTraceResult != null && rayTraceResult.typeOfHit == RayTraceResult.Type.BLOCK && rayTraceResult.sideHit == EnumFacing.UP) {
            val blockPos = rayTraceResult.blockPos
            if (world.getBlockState(blockPos).block == features.enormousBookOpen) {
                val entityPos = BlockPos(MathHelper.floor(entity.posX), MathHelper.floor(entity.posY), MathHelper.floor(entity.posZ))
                return world.getBlockState(entityPos).block == features.enormousBookOpen
            }
        }
        return false
    }

    @SubscribeEvent
    fun onFOVModifier(e: EntityViewRenderEvent.FOVModifier) {
        e.fov *= (1.0F + (MAX_LOOK_TIME - interpolatedLookTime) / MAX_LOOK_TIME.toFloat()) / 2.0F
    }

    @SubscribeEvent
    fun onClientTick(e: TickEvent.ClientTickEvent) {
        val player = Minecraft.getMinecraft().player
        if (player != null) {
            prevLookTime = lookTime
            if (isOnBook(player.world, Minecraft.getMinecraft().objectMouseOver, player)) {
                if (lookTime < MAX_LOOK_TIME) {
                    lookTime++
                }
            } else {
                if (lookTime > 0) {
                    lookTime--
                }
            }

            val factor = scaleFactor(lookTime.toFloat())
            player.setVelocity(player.motionX * factor, player.motionY * factor, player.motionZ * factor)
        }
    }

    @SubscribeEvent
    fun onViewport(e: EntityViewRenderEvent.CameraSetup) {
        val player = e.entity
        if (player is EntityPlayer) {
            player.eyeHeight = player.defaultEyeHeight * scaleFactor(interpolatedLookTime)
        }
    }
}