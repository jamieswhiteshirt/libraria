package com.jamieswhiteshirt.libraria.util

import net.minecraft.util.math.Vec3i

sealed class OctreeNode<T>(val level: Int) {
    class Leaf<T>(val value: T, level: Int) : OctreeNode<T>(level) {
        override fun forEach(action: (T) -> Unit) {
            action(value)
        }

        override fun find(pos: Vec3i): T = value
    }

    class Inner<T>(private val children: Array<OctreeNode<T>>, level: Int) : OctreeNode<T>(level) {
        private val bitIndex = level - 1

        init {
            if (level <= 0) {
                throw RuntimeException("Inner node cannot be on level $level")
            }
        }

        private val Int.bit: Int get () = (this shr bitIndex) and 1

        override fun forEach(action: (T) -> Unit) {
            children.forEach { it.forEach(action) }
        }

        override fun find(pos: Vec3i): T = this[pos].find(pos)

        private fun indexOf(pos: Vec3i): Int = (pos.x.bit shl 2) or (pos.y.bit shl 1) or pos.z.bit

        operator fun get(pos: Vec3i): OctreeNode<T> = this[indexOf(pos)]

        operator fun get(index: Int): OctreeNode<T> = children[index]
    }

    abstract fun forEach(action: (T) -> Unit)

    abstract fun find(pos: Vec3i): T
}

