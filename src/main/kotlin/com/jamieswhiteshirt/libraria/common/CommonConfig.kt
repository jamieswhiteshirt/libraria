package com.jamieswhiteshirt.libraria.common

import net.minecraftforge.common.config.Configuration

abstract class CommonConfig(configuration: Configuration) {
    val biomeId = configuration.getInt("biomeId", "libraria", 127, 0, 127, "Biome ID for the Libraria biome")
    val dimensionId = configuration.getInt("dimensionId", "libraria", 127, 0, 127, "Dimension ID for Libraria")
}