package com.jamieswhiteshirt.libraria.common.biome

import net.minecraft.world.biome.Biome

class BiomeLibrary : Biome(Properties) {
    object Properties : Biome.BiomeProperties("library")
}