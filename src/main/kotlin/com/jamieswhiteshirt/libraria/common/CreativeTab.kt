package com.jamieswhiteshirt.libraria.common

import net.minecraft.creativetab.CreativeTabs
import net.minecraft.item.ItemStack

class CreativeTab(val features: CommonFeatures) : CreativeTabs("libraria.tab") {
    override fun getTabIconItem(): ItemStack = ItemStack(features.enormousBookClosed)
}