package com.jamieswhiteshirt.libraria.common

import net.minecraft.block.state.IBlockState
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

fun World.setBlockStates(posStates: Map<BlockPos, IBlockState>) = setBlockStates(posStates, 3)

fun World.setBlockStates(posStates: Map<BlockPos, IBlockState>, flags: Int) {
    val posStateUpdates = posStates.mapValues { getBlockState(it.key) to it.value }

    posStateUpdates.forEach {
        setBlockState(it.key, it.value.second, 0)
    }
    posStateUpdates.forEach {
        notifyBlockUpdate(it.key, it.value.first, it.value.second, flags)
    }
}
