package com.jamieswhiteshirt.libraria.common.block

import com.jamieswhiteshirt.libraria.api.EnumHorizontalRelativeFacing
import com.jamieswhiteshirt.libraria.common.CommonFeatures
import com.jamieswhiteshirt.libraria.common.setBlockStates
import net.minecraft.block.BlockHorizontal
import net.minecraft.block.material.Material
import net.minecraft.block.state.BlockStateContainer
import net.minecraft.block.state.IBlockState
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumHand
import net.minecraft.util.math.BlockPos
import net.minecraft.world.World

class BlockEnormousBookClosed(val features: CommonFeatures) : BlockHorizontal(Material.WOOD) {
    init {
        blockHardness = 1.0F
    }

    override fun createBlockState(): BlockStateContainer = BlockStateContainer(this, FACING)

    @Suppress("OverridingDeprecatedMember")
    override fun getStateFromMeta(meta: Int): IBlockState = defaultState.withProperty(FACING, EnumFacing.getHorizontal(meta))

    override fun getMetaFromState(state: IBlockState): Int = state.getValue(FACING).horizontalIndex

    override fun getStateForPlacement(world: World, pos: BlockPos, facing: EnumFacing, hitX: Float, hitY: Float, hitZ: Float, meta: Int, placer: EntityLivingBase, hand: EnumHand): IBlockState {
        return defaultState.withProperty(FACING, placer.horizontalFacing)
    }

    fun tryOpen(world: World, pos: BlockPos, state: IBlockState): Boolean {
        val facing = state.getValue(FACING)
        val leftPos = pos.offset(EnumHorizontalRelativeFacing.LEFT.map(facing))
        if (world.getBlockState(leftPos).block.isReplaceable(world, leftPos)) {
            val baseState = features.enormousBookOpen.defaultState.withProperty(FACING, facing)
            world.setBlockStates(mapOf(
                    pos to baseState.withProperty(BlockEnormousBookOpen.SIDE, EnumHorizontalRelativeFacing.RIGHT),
                    leftPos to baseState.withProperty(BlockEnormousBookOpen.SIDE, EnumHorizontalRelativeFacing.LEFT)
            ))
            return true
        } else {
            return false
        }
    }

    override fun onBlockActivated(world: World, pos: BlockPos, state: IBlockState, player: EntityPlayer, hand: EnumHand, facing: EnumFacing, hitX: Float, hitY: Float, hitZ: Float): Boolean {
        return tryOpen(world, pos, state)
    }
}
