package com.jamieswhiteshirt.libraria.common.block

import net.minecraft.block.BlockStairs
import net.minecraft.block.state.IBlockState

class BlockModStairs(modelState: IBlockState) : BlockStairs(modelState)
