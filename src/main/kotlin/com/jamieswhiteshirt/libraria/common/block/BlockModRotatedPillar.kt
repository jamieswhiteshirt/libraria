package com.jamieswhiteshirt.libraria.common.block

import net.minecraft.block.BlockRotatedPillar
import net.minecraft.block.material.MapColor
import net.minecraft.block.material.Material

class BlockModRotatedPillar(materialIn: Material, color: MapColor) : BlockRotatedPillar(materialIn, color)
