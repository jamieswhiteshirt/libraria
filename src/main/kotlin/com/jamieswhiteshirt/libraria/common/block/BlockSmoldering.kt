package com.jamieswhiteshirt.libraria.common.block

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.block.state.IBlockState
import net.minecraft.util.BlockRenderLayer
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumParticleTypes
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d
import net.minecraft.world.IBlockAccess
import net.minecraft.world.World
import net.minecraftforge.client.MinecraftForgeClient
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly
import java.util.*

class BlockSmoldering : Block(Material.WOOD) {
    init {
        setLightLevel(1.0F / 15.0F)
    }

    override fun getLightValue(state: IBlockState, world: IBlockAccess, pos: BlockPos): Int {
        return if (MinecraftForgeClient.getRenderLayer() == BlockRenderLayer.CUTOUT) 1 else 0
    }

    override fun canRenderInLayer(state: IBlockState, layer: BlockRenderLayer): Boolean {
        return layer == BlockRenderLayer.SOLID || layer == BlockRenderLayer.CUTOUT
    }

    @Suppress("OverridingDeprecatedMember")
    @SideOnly(Side.CLIENT)
    override fun getPackedLightmapCoords(state: IBlockState, source: IBlockAccess, pos: BlockPos): Int {
        if (MinecraftForgeClient.getRenderLayer() == BlockRenderLayer.CUTOUT) {
            val result = source.getCombinedLight(pos, 1)
            val skyLight = (result shr 16) and 0xFFFF
            return (skyLight shl 16) or (15 shl 4)
        } else {
            @Suppress("DEPRECATION")
            return super.getPackedLightmapCoords(state, source, pos)
        }
    }

    @SideOnly(Side.CLIENT)
    override fun randomDisplayTick(state: IBlockState, world: World, pos: BlockPos, rand: Random) {
        if (rand.nextInt(10) != 0) {
            return
        }
        val facing = EnumFacing.getFront(rand.nextInt(6))

        if (facing == EnumFacing.UP) {
            return
        }
        val material = world.getBlockState(pos.offset(facing)).material
        if (material.blocksMovement() || material.isLiquid) {
            return
        }

        val axis = facing.axis!!
        val perpendicularAxes = EnumFacing.Axis.values().filter { it != axis }

        val baseOffset = Vec3d(facing.directionVec).scale(if (facing.axisDirection == EnumFacing.AxisDirection.POSITIVE) {
            1.15
        } else {
            0.15
        })

        val position = Vec3d(pos).add(perpendicularAxes.fold(baseOffset) { acc, it ->
            acc.add(Vec3d(EnumFacing.getFacingFromAxis(EnumFacing.AxisDirection.POSITIVE, it).directionVec).scale(rand.nextDouble()))
        })

        world.spawnParticle(EnumParticleTypes.BLOCK_DUST, position.xCoord, position.yCoord, position.zCoord, 0.0, 0.0, 0.0, Block.getStateId(state))
    }
}
