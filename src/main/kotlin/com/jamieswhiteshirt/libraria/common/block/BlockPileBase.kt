package com.jamieswhiteshirt.libraria.common.block

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.block.state.IBlockState
import net.minecraft.util.EnumFacing
import net.minecraft.util.math.AxisAlignedBB
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess
import net.minecraft.world.World

abstract class BlockPileBase(materialIn: Material?) : Block(materialIn) {
    companion object {
        val AABB = AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 1.0 / 16.0, 1.0)
    }

    @Suppress("OverridingDeprecatedMember")
    override fun getBoundingBox(state: IBlockState, source: IBlockAccess, pos: BlockPos): AxisAlignedBB = AABB

    @Suppress("OverridingDeprecatedMember")
    override fun getCollisionBoundingBox(blockState: IBlockState, world: IBlockAccess, pos: BlockPos): AxisAlignedBB? = NULL_AABB

    @Suppress("OverridingDeprecatedMember")
    override fun isOpaqueCube(state: IBlockState): Boolean = false

    @Suppress("OverridingDeprecatedMember")
    override fun isFullCube(state: IBlockState): Boolean = false

    override fun canPlaceBlockAt(world: World, pos: BlockPos): Boolean {
        val downPos = pos.down()
        return world.getBlockState(downPos).isSideSolid(world, downPos, EnumFacing.UP)
    }

    @Suppress("OverridingDeprecatedMember")
    override fun neighborChanged(state: IBlockState, world: World, pos: BlockPos, block: Block, fromPos: BlockPos) {
        checkAndDropBlock(world, pos)
    }

    private fun checkAndDropBlock(world: World, pos: BlockPos) {
        if (!canPlaceBlockAt(world, pos)) {
            world.setBlockToAir(pos)
        }
    }
}
