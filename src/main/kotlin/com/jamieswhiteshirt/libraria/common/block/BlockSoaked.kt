package com.jamieswhiteshirt.libraria.common.block

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.block.state.IBlockState
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumParticleTypes
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3d
import net.minecraft.world.World
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly
import java.util.*

class BlockSoaked : Block(Material.WOOD) {
    @SideOnly(Side.CLIENT)
    override fun randomDisplayTick(state: IBlockState, world: World, pos: BlockPos, rand: Random) {
        if (rand.nextInt(10) != 0) {
            return
        }
        val facing = EnumFacing.getFront(rand.nextInt(6))

        if (facing == EnumFacing.UP) {
            return
        }
        val material = world.getBlockState(pos.offset(facing)).material
        if (material.blocksMovement() || material.isLiquid) {
            return
        }

        val axis = facing.axis!!
        val perpendicularAxes = EnumFacing.Axis.values().filter { it != axis }

        val baseOffset = Vec3d(facing.directionVec).scale(if (facing.axisDirection == EnumFacing.AxisDirection.POSITIVE) {
            1.15
        } else {
            0.0
        })

        val position = Vec3d(pos).add(perpendicularAxes.fold(baseOffset) { acc, it ->
            acc.add(Vec3d(EnumFacing.getFacingFromAxis(EnumFacing.AxisDirection.POSITIVE, it).directionVec).scale(rand.nextDouble()))
        })

        world.spawnParticle(EnumParticleTypes.DRIP_WATER, position.xCoord, position.yCoord, position.zCoord, 0.0, 0.0, 0.0)
    }
}
