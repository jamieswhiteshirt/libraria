package com.jamieswhiteshirt.libraria.common.block

import net.minecraft.block.material.Material
import net.minecraft.util.BlockRenderLayer
import net.minecraftforge.fml.relauncher.Side
import net.minecraftforge.fml.relauncher.SideOnly

class BlockPuddle : BlockPileBase(Material.CIRCUITS) {
    @SideOnly(Side.CLIENT)
    override fun getBlockLayer(): BlockRenderLayer = BlockRenderLayer.TRANSLUCENT
}
