package com.jamieswhiteshirt.libraria.common.block

import com.jamieswhiteshirt.libraria.api.EnumHorizontalRelativeFacing
import com.jamieswhiteshirt.libraria.common.CommonFeatures
import com.jamieswhiteshirt.libraria.common.setBlockStates
import com.jamieswhiteshirt.libraria.common.world.TeleporterLibraria
import net.minecraft.block.Block
import net.minecraft.block.BlockHorizontal
import net.minecraft.block.material.Material
import net.minecraft.block.properties.PropertyEnum
import net.minecraft.block.state.BlockStateContainer
import net.minecraft.block.state.IBlockState
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.entity.player.EntityPlayerMP
import net.minecraft.init.Blocks
import net.minecraft.item.Item
import net.minecraft.util.EntitySelectors
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumHand
import net.minecraft.util.math.AxisAlignedBB
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess
import net.minecraft.world.World
import net.minecraft.world.WorldServer
import java.util.*

class BlockEnormousBookOpen(val features: CommonFeatures) : BlockHorizontal(Material.WOOD) {
    companion object {
        val SIDE = PropertyEnum.create<EnumHorizontalRelativeFacing>("side", EnumHorizontalRelativeFacing::class.java)
        val AABB_HALF = AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 0.5, 1.0)
    }

    init {
        blockHardness = 1.0F
    }

    @Suppress("OverridingDeprecatedMember")
    override fun getBoundingBox(state: IBlockState, source: IBlockAccess, pos: BlockPos): AxisAlignedBB = AABB_HALF

    @Suppress("OverridingDeprecatedMember")
    override fun isFullyOpaque(state: IBlockState): Boolean = false

    @Suppress("OverridingDeprecatedMember")
    override fun isOpaqueCube(state: IBlockState): Boolean = false

    @Suppress("OverridingDeprecatedMember")
    override fun isFullCube(state: IBlockState): Boolean = false

    override fun createBlockState(): BlockStateContainer = BlockStateContainer(this, BlockHorizontal.FACING, SIDE)

    @Suppress("OverridingDeprecatedMember")
    override fun getStateFromMeta(meta: Int): IBlockState {
        return defaultState.withProperty(FACING, EnumFacing.getHorizontal(meta and 3)).withProperty(SIDE, EnumHorizontalRelativeFacing.getFacing((meta shr 2) and 1))
    }

    override fun getMetaFromState(state: IBlockState): Int {
        return state.getValue(FACING).horizontalIndex or (state.getValue(SIDE).ordinal shl 2)
    }

    fun tryClose(world: World, pos: BlockPos, state: IBlockState): Boolean {
        val facing = state.getValue(FACING)
        val side = state.getValue(SIDE)!!
        val otherFacing = side.opposite.map(facing)
        val otherPos = pos.offset(otherFacing)
        val otherState = world.getBlockState(otherPos)
        if (otherState == state.withProperty(SIDE, side.opposite)) {
            val closedState = features.enormousBookClosed.defaultState.withProperty(FACING, facing)
            world.setBlockStates(when (side) {
                EnumHorizontalRelativeFacing.LEFT -> mapOf(
                        pos to Blocks.AIR.defaultState,
                        otherPos to closedState
                )
                EnumHorizontalRelativeFacing.RIGHT -> mapOf(
                        pos to closedState,
                        otherPos to Blocks.AIR.defaultState
                )
            })

            if (world is WorldServer) {
                val playerList = world.minecraftServer?.playerList
                if (playerList != null) {
                    val teleporter = TeleporterLibraria(world)
                    val boundingBox = AxisAlignedBB(pos, otherPos).expandXyz(0.5).offset(0.5, 1.0, 0.5)

                    world.getEntitiesInAABBexcluding(null, boundingBox, EntitySelectors.IS_ALIVE).forEach {
                        it.setPortal(pos)
                        if (it is EntityPlayerMP) {
                            playerList.transferPlayerToDimension(it, features.dimensionType.id, teleporter)
                        }
                        //it.changeDimension(features.dimensionType.id)
                    }
                }
            }

            return true
        } else {
            return false
        }
    }

    override fun onBlockActivated(world: World, pos: BlockPos, state: IBlockState, player: EntityPlayer, hand: EnumHand, facing: EnumFacing, hitX: Float, hitY: Float, hitZ: Float): Boolean {
        return tryClose(world, pos, state)
    }

    @Suppress("OverridingDeprecatedMember")
    override fun neighborChanged(state: IBlockState, world: World, pos: BlockPos, block: Block, fromPos: BlockPos) {
        if (!world.isRemote) {
            checkAndDropBlock(world, pos, state)
        }
    }

    override fun getItemDropped(state: IBlockState, rand: Random, fortune: Int): Item = Item.getItemFromBlock(features.enormousBookClosed)

    override fun removedByPlayer(state: IBlockState, world: World, pos: BlockPos, player: EntityPlayer, willHarvest: Boolean): Boolean {
        removeSafely(world, pos, state)
        return true
    }

    fun checkAndDropBlock(world: World, pos: BlockPos, state: IBlockState) {
        if (!canBlockStay(world, pos, state)) {
            dropBlockAsItem(world, pos, state, 0)
            removeSafely(world, pos, state)
        }
    }

    fun canBlockStay(world: World, pos: BlockPos, state: IBlockState): Boolean {
        val facing = state.getValue(FACING)
        val side = state.getValue(SIDE)
        val otherPos = pos.offset(side.map(facing))
        return world.getBlockState(otherPos) == state.withProperty(SIDE, side.opposite)
    }

    fun removeSafely(world: World, pos: BlockPos, state: IBlockState) {
        val facing = state.getValue(FACING)
        val side = state.getValue(SIDE)
        val otherPos = pos.offset(side.opposite.map(facing))
        val otherState = world.getBlockState(otherPos)

        world.setBlockStates(if (otherState == state.withProperty(SIDE, side.opposite)) {
            listOf(pos, otherPos)
        } else {
            listOf(pos)
        }.associate { it to Blocks.AIR.defaultState })
    }
}
