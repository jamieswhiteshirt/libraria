package com.jamieswhiteshirt.libraria.common.block

import net.minecraft.block.BlockHorizontal
import net.minecraft.block.state.BlockStateContainer
import net.minecraft.block.state.IBlockState
import net.minecraft.entity.EntityLivingBase
import net.minecraft.util.EnumFacing
import net.minecraft.util.EnumHand
import net.minecraft.util.Mirror
import net.minecraft.util.Rotation
import net.minecraft.util.math.AxisAlignedBB
import net.minecraft.util.math.BlockPos
import net.minecraft.world.IBlockAccess
import net.minecraft.world.World

class BlockRailing(modelState: IBlockState) : BlockHorizontal(modelState.material) {
    companion object {
        val FACING = BlockHorizontal.FACING

        val AABBs = arrayOf(
            AxisAlignedBB(0.0, 0.0, 0.75, 1.0, 1.0, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 0.25, 1.0, 1.0),
            AxisAlignedBB(0.0, 0.0, 0.0, 1.0, 1.0, 0.25),
            AxisAlignedBB(0.75, 0.0, 0.0, 1.0, 1.0, 1.0)
        )
    }

    init {
        defaultState = blockState.baseState.withProperty(FACING, EnumFacing.SOUTH)
    }

    override fun getStateForPlacement(world: World, pos: BlockPos, facing: EnumFacing, hitX: Float, hitY: Float, hitZ: Float, meta: Int, placer: EntityLivingBase, hand: EnumHand): IBlockState {
        val placementFacing = EnumFacing.getFacingFromVector(hitX - 0.5F - facing.directionVec.x, 0.0F, hitZ - 0.5F - facing.directionVec.z)
        return if (placementFacing.axis != EnumFacing.Axis.Y) {
            defaultState.withProperty(FACING, placementFacing)
        } else {
            defaultState
        }
    }

    override fun createBlockState(): BlockStateContainer {
        return BlockStateContainer(this, FACING)
    }

    @Suppress("OverridingDeprecatedMember")
    override fun getBoundingBox(state: IBlockState, source: IBlockAccess, pos: BlockPos): AxisAlignedBB {
        return AABBs[state.getValue(FACING).horizontalIndex]
    }

    @Suppress("OverridingDeprecatedMember")
    override fun getStateFromMeta(meta: Int): IBlockState {
        return defaultState.withProperty(FACING, EnumFacing.getHorizontal(meta))
    }

    override fun getMetaFromState(state: IBlockState): Int {
        return state.getValue(FACING).horizontalIndex
    }

    @Suppress("OverridingDeprecatedMember")
    override fun isOpaqueCube(state: IBlockState): Boolean = false

    @Suppress("OverridingDeprecatedMember")
    override fun isFullCube(state: IBlockState): Boolean = false

    override fun isPassable(worldIn: IBlockAccess, pos: BlockPos): Boolean = false

    @Suppress("OverridingDeprecatedMember")
    override fun withRotation(state: IBlockState, rot: Rotation): IBlockState {
        return state.withProperty(FACING, rot.rotate(state.getValue(FACING)))
    }

    @Suppress("OverridingDeprecatedMember")
    override fun withMirror(state: IBlockState, mirror: Mirror): IBlockState {
        return state.withProperty(FACING, mirror.mirror(state.getValue(FACING)))
    }
}