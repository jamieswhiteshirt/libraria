package com.jamieswhiteshirt.libraria.common.block

import net.minecraft.block.Block
import net.minecraft.block.material.Material
import net.minecraft.block.state.IBlockState
import net.minecraft.init.Items
import net.minecraft.item.Item
import java.util.*

class BlockMarmorBookshelf : Block(Material.ROCK) {
    override fun quantityDropped(random: Random): Int = 3

    override fun getItemDropped(state: IBlockState, rand: Random, fortune: Int): Item = Items.BOOK
}