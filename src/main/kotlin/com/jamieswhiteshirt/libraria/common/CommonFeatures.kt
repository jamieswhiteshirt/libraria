package com.jamieswhiteshirt.libraria.common

import com.jamieswhiteshirt.libraria.common.biome.BiomeLibrary
import com.jamieswhiteshirt.libraria.common.block.*
import com.jamieswhiteshirt.libraria.common.world.WorldProviderLibraria
import net.minecraft.block.Block
import net.minecraft.block.BlockPlanks
import net.minecraft.block.material.MapColor
import net.minecraft.block.material.Material
import net.minecraft.init.Blocks
import net.minecraft.item.ItemBlock
import net.minecraft.util.ResourceLocation
import net.minecraft.world.DimensionType
import net.minecraft.world.biome.Biome
import net.minecraftforge.common.DimensionManager
import net.minecraftforge.fml.common.registry.GameRegistry

abstract class CommonFeatures(config: CommonConfig) {
    val creativeTab = CreativeTab(this)

    val biome = BiomeLibrary()
    val dimensionType: DimensionType

    val enormousBookClosed: BlockEnormousBookClosed = BlockEnormousBookClosed(this).setUnlocalizedName("libraria.enormousBook").setCreativeTab(creativeTab) as BlockEnormousBookClosed
    val enormousBookOpen: BlockEnormousBookOpen = BlockEnormousBookOpen(this).setUnlocalizedName("libraria.enormousBook") as BlockEnormousBookOpen
    val charredBookshelf: BlockCharred = BlockCharred().setUnlocalizedName("libraria.charredBookshelf").setCreativeTab(creativeTab) as BlockCharred
    val charredPlanks: BlockCharred = BlockCharred().setUnlocalizedName("libraria.charredPlanks").setCreativeTab(creativeTab) as BlockCharred
    val smolderingBookshelf: BlockSmoldering = BlockSmoldering().setUnlocalizedName("libraria.smolderingBookshelf").setCreativeTab(creativeTab) as BlockSmoldering
    val smolderingPlanks: BlockSmoldering = BlockSmoldering().setUnlocalizedName("libraria.smolderingPlanks").setCreativeTab(creativeTab) as BlockSmoldering
    val paperPile: BlockPile = BlockPile().setUnlocalizedName("libraria.paperPile").setCreativeTab(creativeTab) as BlockPile
    val ashPile: BlockPile = BlockPile().setUnlocalizedName("libraria.ashPile").setCreativeTab(creativeTab) as BlockPile
    val marmor: Block = Block(Material.ROCK).setUnlocalizedName("libraria.marmor").setCreativeTab(creativeTab)
    val marmorTile: Block = Block(Material.ROCK).setUnlocalizedName("libraria.marmorTile").setCreativeTab(creativeTab)
    val marmorTileStairs: Block = BlockModStairs(marmorTile.defaultState).setUnlocalizedName("libraria.stairsMarmorTile").setCreativeTab(creativeTab)
    val marmorRailing: Block = BlockRailing(marmor.defaultState).setUnlocalizedName("libraria.marmorRailing").setCreativeTab(creativeTab)
    val marmorPillar: Block = BlockModRotatedPillar(Material.ROCK, MapColor.SNOW).setUnlocalizedName("libraria.marmorPillar").setCreativeTab(creativeTab)
    val marmorBookshelf: Block = BlockMarmorBookshelf().setUnlocalizedName("libraria.marmorBookshelf").setCreativeTab(creativeTab)
    val oakRailing: Block = BlockRailing(Blocks.PLANKS.defaultState.withProperty(BlockPlanks.VARIANT, BlockPlanks.EnumType.OAK)).setUnlocalizedName("libraria.oakRailing").setCreativeTab(creativeTab)
    val wetBookshelf: BlockWet = BlockWet().setUnlocalizedName("libraria.wetBookshelf").setCreativeTab(creativeTab) as BlockWet
    val wetPlanks: BlockWet = BlockWet().setUnlocalizedName("libraria.wetPlanks").setCreativeTab(creativeTab) as BlockWet
    val soakedBookshelf: BlockSoaked = BlockSoaked().setUnlocalizedName("libraria.soakedBookshelf").setCreativeTab(creativeTab) as BlockSoaked
    val soakedPlanks: BlockSoaked = BlockSoaked().setUnlocalizedName("libraria.soakedPlanks").setCreativeTab(creativeTab) as BlockSoaked
    val puddle: BlockPuddle = BlockPuddle().setUnlocalizedName("libraria.puddle").setCreativeTab(creativeTab) as BlockPuddle

    init {
        Biome.registerBiome(config.biomeId, "libraria:library", biome)

        dimensionType = DimensionType.register("libraria", "_libraria", config.dimensionId, WorldProviderLibraria::class.java, false)
        DimensionManager.registerDimension(config.dimensionId, dimensionType)
    }

    fun register() {
        GameRegistry.register(enormousBookClosed, ResourceLocation("libraria", "enormous_book_closed"))
        GameRegistry.register(ItemBlock(enormousBookClosed), enormousBookClosed.registryName)
        GameRegistry.register(enormousBookOpen, ResourceLocation("libraria", "enormous_book_open"))
        GameRegistry.register(charredBookshelf, ResourceLocation("libraria", "charred_bookshelf"))
        GameRegistry.register(ItemBlock(charredBookshelf), charredBookshelf.registryName)
        GameRegistry.register(charredPlanks, ResourceLocation("libraria", "charred_planks"))
        GameRegistry.register(ItemBlock(charredPlanks), charredPlanks.registryName)
        GameRegistry.register(smolderingBookshelf, ResourceLocation("libraria", "smoldering_bookshelf"))
        GameRegistry.register(ItemBlock(smolderingBookshelf), smolderingBookshelf.registryName)
        GameRegistry.register(smolderingPlanks, ResourceLocation("libraria", "smoldering_planks"))
        GameRegistry.register(ItemBlock(smolderingPlanks), smolderingPlanks.registryName)
        GameRegistry.register(paperPile, ResourceLocation("libraria", "paper_pile"))
        GameRegistry.register(ItemBlock(paperPile), paperPile.registryName)
        GameRegistry.register(ashPile, ResourceLocation("libraria", "ash_pile"))
        GameRegistry.register(ItemBlock(ashPile), ashPile.registryName)
        GameRegistry.register(marmor, ResourceLocation("libraria", "marmor"))
        GameRegistry.register(ItemBlock(marmor), marmor.registryName)
        GameRegistry.register(marmorTile, ResourceLocation("libraria", "marmor_tile"))
        GameRegistry.register(ItemBlock(marmorTile), marmorTile.registryName)
        GameRegistry.register(marmorTileStairs, ResourceLocation("libraria", "marmor_tile_stairs"))
        GameRegistry.register(ItemBlock(marmorTileStairs), marmorTileStairs.registryName)
        GameRegistry.register(marmorRailing, ResourceLocation("libraria", "marmor_railing"))
        GameRegistry.register(ItemBlock(marmorRailing), marmorRailing.registryName)
        GameRegistry.register(marmorPillar, ResourceLocation("libraria", "marmor_pillar"))
        GameRegistry.register(ItemBlock(marmorPillar), marmorPillar.registryName)
        GameRegistry.register(marmorBookshelf, ResourceLocation("libraria", "marmor_bookshelf"))
        GameRegistry.register(ItemBlock(marmorBookshelf), marmorBookshelf.registryName)
        GameRegistry.register(oakRailing, ResourceLocation("libraria", "oak_railing"))
        GameRegistry.register(ItemBlock(oakRailing), oakRailing.registryName)
        GameRegistry.register(wetBookshelf, ResourceLocation("libraria", "wet_bookshelf"))
        GameRegistry.register(ItemBlock(wetBookshelf), wetBookshelf.registryName)
        GameRegistry.register(wetPlanks, ResourceLocation("libraria", "wet_planks"))
        GameRegistry.register(ItemBlock(wetPlanks), wetPlanks.registryName)
        GameRegistry.register(soakedBookshelf, ResourceLocation("libraria", "soaked_bookshelf"))
        GameRegistry.register(ItemBlock(soakedBookshelf), soakedBookshelf.registryName)
        GameRegistry.register(soakedPlanks, ResourceLocation("libraria", "soaked_planks"))
        GameRegistry.register(ItemBlock(soakedPlanks), soakedPlanks.registryName)
        GameRegistry.register(puddle, ResourceLocation("libraria", "puddle"))
        GameRegistry.register(ItemBlock(puddle), puddle.registryName)
    }

    open fun registerEventHandlers() {}
}
