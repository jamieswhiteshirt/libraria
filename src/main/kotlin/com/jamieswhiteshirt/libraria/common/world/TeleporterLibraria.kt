package com.jamieswhiteshirt.libraria.common.world

import net.minecraft.entity.Entity
import net.minecraft.world.Teleporter
import net.minecraft.world.WorldServer

class TeleporterLibraria(world: WorldServer) : Teleporter(world) {
    override fun makePortal(entityIn: Entity): Boolean {
        return true
    }
}