package com.jamieswhiteshirt.libraria.common.world.structure

import com.jamieswhiteshirt.libraria.common.world.section.Section
import com.jamieswhiteshirt.libraria.common.world.section.SectionBookshelfRoomEdge
import com.jamieswhiteshirt.libraria.common.world.section.SectionNothing
import net.minecraft.block.state.IBlockState
import net.minecraft.util.math.Vec3i

class StructureBookshelfRoom(val type: IBlockState, pos: Vec3i, size: Int) : Structure(pos, size) {
    override fun get(pos: Vec3i): Section {
        return if (pos.y == this.pos.y) {
            SectionBookshelfRoomEdge(type)
        } else {
            SectionNothing
        }
    }
}