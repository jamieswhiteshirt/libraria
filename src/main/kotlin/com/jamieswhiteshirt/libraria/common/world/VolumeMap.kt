package com.jamieswhiteshirt.libraria.common.world

import com.jamieswhiteshirt.libraria.common.CommonFeatures
import com.jamieswhiteshirt.libraria.common.world.structure.Structure
import com.jamieswhiteshirt.libraria.util.OctreeNode
import net.minecraft.init.Blocks
import net.minecraft.util.math.Vec3i

data class VolumeChunkPos(val x: Int, val z: Int)

class VolumeMap(features: CommonFeatures, seed: Long, val octreeDepth: Int) {
    private val provider = StructureChunkProvider(features, seed, octreeDepth)
    private val chunks = HashMap<VolumeChunkPos, OctreeNode<Structure>>()

    operator fun get(pos: Vec3i): Structure {
        val chunkPos = VolumeChunkPos(pos.x shr octreeDepth, pos.z shr octreeDepth)
        return chunks.getOrPut(chunkPos, { provider.provideStructureChunk(chunkPos) }).find(pos)
    }
}