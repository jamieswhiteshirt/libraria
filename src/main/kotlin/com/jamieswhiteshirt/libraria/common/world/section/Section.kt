package com.jamieswhiteshirt.libraria.common.world.section

import net.minecraft.world.chunk.ChunkPrimer

abstract class Section {
    abstract fun provide(primer: ChunkPrimer, fromX: Int, fromY: Int, fromZ: Int)
}
