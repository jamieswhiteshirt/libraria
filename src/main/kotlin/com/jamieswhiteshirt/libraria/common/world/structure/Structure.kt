package com.jamieswhiteshirt.libraria.common.world.structure

import com.jamieswhiteshirt.libraria.common.world.section.Section
import net.minecraft.block.state.IBlockState
import net.minecraft.util.math.Vec3i

abstract class Structure(val pos: Vec3i, val size: Int) {
    abstract operator fun get(pos: Vec3i): Section
}
