package com.jamieswhiteshirt.libraria.common.world.section

import net.minecraft.world.chunk.ChunkPrimer

object SectionNothing : Section() {
    override fun provide(primer: ChunkPrimer, fromX: Int, fromY: Int, fromZ: Int) {}
}