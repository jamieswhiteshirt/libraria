package com.jamieswhiteshirt.libraria.common.world

import com.jamieswhiteshirt.libraria.common.CommonFeatures
import net.minecraft.entity.EnumCreatureType
import net.minecraft.init.Blocks
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3i
import net.minecraft.world.World
import net.minecraft.world.biome.Biome
import net.minecraft.world.chunk.Chunk
import net.minecraft.world.chunk.ChunkPrimer
import net.minecraft.world.chunk.IChunkGenerator

class ChunkProviderLibraria(val features: CommonFeatures, val world: World) : IChunkGenerator {
    companion object {
        const val OCTREE_DEPTH = 3
    }

    private val structureMap = VolumeMap(features, world.seed, OCTREE_DEPTH)

    override fun generateStructures(chunk: Chunk, x: Int, z: Int): Boolean = false

    override fun getPossibleCreatures(creatureType: EnumCreatureType, pos: BlockPos): MutableList<Biome.SpawnListEntry> {
        return world.getBiome(pos).getSpawnableList(creatureType)
    }

    override fun populate(x: Int, z: Int) {}

    override fun recreateStructures(chunk: Chunk, x: Int, z: Int) {}

    private fun provideStructures(chunkX: Int, chunkZ: Int) {
        val baseX = chunkX shl 1
        val baseZ = chunkZ shl 1
        for (subX in 0..1) {
            val x = baseX + subX
            for (subZ in 0..1) {
                val z = baseZ + subZ
                for (y in 0..7) {
                    val pos = Vec3i(x, y, z)
                    val structure = structureMap[pos]
                }
            }
        }
    }

    override fun provideChunk(chunkX: Int, chunkZ: Int): Chunk {
        val primer = ChunkPrimer()

        val baseX = chunkX shl 1
        val baseZ = chunkZ shl 1
        for (subX in 0..1) {
            for (subZ in 0..1) {
                for (y in 0..7) {
                    val pos = Vec3i(baseX + subX, y, baseZ + subZ)
                    val structure = structureMap[pos]
                    val section = structure[pos]
                    section.provide(primer, subX * 8, y * 8, subZ * 8)
                }
            }
        }

        for (xPos in 0..15) {
            for (zPos in 0..15) {
                primer.setBlockState(xPos, 0, zPos, Blocks.BEDROCK.defaultState)
                primer.setBlockState(xPos, 1, zPos, features.marmor.defaultState)
                primer.setBlockState(xPos, 62, zPos, features.marmor.defaultState)
                primer.setBlockState(xPos, 63, zPos, Blocks.BEDROCK.defaultState)
            }
        }

        val chunk = Chunk(world, primer, chunkX, chunkZ)
        chunk.generateSkylightMap()
        return chunk
    }

    override fun getStrongholdGen(world: World, structureName: String, position: BlockPos, p_180513_4_: Boolean): BlockPos? = null
}
