package com.jamieswhiteshirt.libraria.common.world.section

import net.minecraft.block.state.IBlockState
import net.minecraft.world.chunk.ChunkPrimer

class SectionBookshelfRoomEdge(val state: IBlockState) : Section() {
    override fun provide(primer: ChunkPrimer, fromX: Int, fromY: Int, fromZ: Int) {
        for (x in fromX..(fromX + 7)) {
            for (z in fromZ..(fromZ + 7)) {
                primer.setBlockState(x, fromY, z, state)
            }
        }
    }
}