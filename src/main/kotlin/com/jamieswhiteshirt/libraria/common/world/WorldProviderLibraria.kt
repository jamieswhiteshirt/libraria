package com.jamieswhiteshirt.libraria.common.world

import com.jamieswhiteshirt.libraria.Libraria
import com.jamieswhiteshirt.libraria.common.CommonFeatures
import net.minecraft.world.DimensionType
import net.minecraft.world.WorldProvider
import net.minecraft.world.chunk.IChunkGenerator
import net.minecraftforge.client.IRenderHandler

class WorldProviderLibraria : WorldProvider() {
    private val features: CommonFeatures = Libraria.proxy.features

    override fun isSurfaceWorld(): Boolean = false

    override fun isSkyColored(): Boolean = false

    override fun getCloudRenderer(): IRenderHandler? = null

    override fun getDimensionType(): DimensionType = features.dimensionType

    override fun getWelcomeMessage(): String = "Fallng into Libraria"

    override fun getDepartMessage(): String = "Falling out of Libraria"

    override fun createChunkGenerator(): IChunkGenerator = ChunkProviderLibraria(features, world)
}