package com.jamieswhiteshirt.libraria.common.world

import com.jamieswhiteshirt.libraria.common.CommonFeatures
import com.jamieswhiteshirt.libraria.common.world.structure.Structure
import com.jamieswhiteshirt.libraria.common.world.structure.StructureBookshelfRoom
import com.jamieswhiteshirt.libraria.util.OctreeNode
import net.minecraft.init.Blocks
import net.minecraft.util.math.Vec3i
import java.util.*

class StructureChunkProvider(val features: CommonFeatures, val seed: Long, val octreeDepth: Int) {
    private fun generateLeafNode(rand: Random, pos: Vec3i, level: Int): OctreeNode.Leaf<Structure> {
        val size = 1 shl level
        return OctreeNode.Leaf(StructureBookshelfRoom(when (rand.nextInt(3)) {
            0 -> features.smolderingBookshelf
            1 -> features.soakedBookshelf
            else -> Blocks.BOOKSHELF
        }.defaultState, pos, size), level)
    }

    private fun generateInnerNode(rand: Random, pos: Vec3i, level: Int): OctreeNode.Inner<Structure> {
        val nextLevel = level - 1
        val i = 1 shl nextLevel
        return OctreeNode.Inner(arrayOf(
                generateNode(rand, pos, nextLevel),
                generateNode(rand, Vec3i(pos.x + 0, pos.y + 0, pos.z + i), nextLevel),
                generateNode(rand, Vec3i(pos.x + 0, pos.y + i, pos.z + 0), nextLevel),
                generateNode(rand, Vec3i(pos.x + 0, pos.y + i, pos.z + i), nextLevel),
                generateNode(rand, Vec3i(pos.x + i, pos.y + 0, pos.z + 0), nextLevel),
                generateNode(rand, Vec3i(pos.x + i, pos.y + 0, pos.z + i), nextLevel),
                generateNode(rand, Vec3i(pos.x + i, pos.y + i, pos.z + 0), nextLevel),
                generateNode(rand, Vec3i(pos.x + i, pos.y + i, pos.z + i), nextLevel)
        ), level)
    }

    private fun generateNode(rand: Random, pos: Vec3i, level: Int): OctreeNode<Structure> {
        val size = 1 shl level
        return if (rand.nextInt(size) == 0) {
            generateLeafNode(rand, pos, level)
        } else {
            generateInnerNode(rand, pos, level)
        }
    }

    fun provideStructureChunk(pos: VolumeChunkPos): OctreeNode<Structure> {
        val random = Random(seed xor (pos.hashCode().toLong() shl 16))
        val nodePos = Vec3i(pos.x shl octreeDepth, 0, pos.z shl octreeDepth)
        return generateNode(random, nodePos, octreeDepth)
    }
}
