package com.jamieswhiteshirt.libraria.common

import net.minecraftforge.common.config.Configuration
import net.minecraftforge.fml.common.event.FMLInitializationEvent
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent

abstract class CommonProxy {
    abstract val config: CommonConfig
    abstract val features: CommonFeatures

    abstract fun loadConfiguration(configuration: Configuration)

    abstract fun loadFeatures()

    open fun preInit(e: FMLPreInitializationEvent) {
        Configuration(e.suggestedConfigurationFile).apply {
            loadConfiguration(this)
            save()
        }
        loadFeatures()

        features.register()
        features.registerEventHandlers()
    }

    open fun init(e: FMLInitializationEvent) {

    }

    open fun postInit(e: FMLPostInitializationEvent) {

    }
}
